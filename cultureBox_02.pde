int nbElts = 0;
int nbCategories = 7;
String currentDir;            //×------------- chemin d'accès du dossier de base à partir duquel la recherche s'effectue
File directory;                //×------------ variable pour référencer le dossier de base dans à partir duquel la recherche s'effectue
GUI gui;                      //×-------------  interface graphique pour choisir le type de média à afficher
Bouton [] categoriesMedia = new Bouton [nbCategories];// les boutons pour selectionner les catégories à afficher
String [] nomsCategoriesMedia = {
  "3D", "audio", "video", "image", "archive", "texte", "code"
};
color [] couleurCategoriesMedia = {#FF8C0F, #13EACC, #13EA4E, #EA13CE, #EA6913, #2F13EA, #88E89D};
ArrayList<Element> elements;  //×------------- liste dynamique qui contient chaque element, correspondant à chaque représentation de fichier détecté
StringDict types;            //×-------------- dictionnaire de String pour référencer toutes les catégories de média
StringList chemins;          //×-------------- accumule au fur et à mesure les différents chemin d'accès possible des fichiers détectés


// ------------------------------------------------------------------------------
// Types de documents
String [] _3D = {
  "XCF", "BLEND", "STL", "DXF", "3DS", "DWG"
};
String [] _audio= {
  "RIFF", "WAV", "BWF", "OGG", "AIFF", "CAF", "RAW", "PCM", "CDA", "FLAC", "ALAC", "AC3", "MP3", "WMA"
};
String [] _video= {
  "AVI", "MPEG", "RTC", "WMV", "MOV", "FLV"
};
String [] _texte= {
  "ODT", "TXT", "DOC", "DOCX", "RTF"
};
String [] _code = {
  "PDE", "C", "JS", "JAVA", "PL", "PY", "PHP"
};
String [] _image = {
  "BMP", "JPG", "JPEG", "DXF", "EPS", "GIF", "PCX", "PICT", "TIFF", "WPG", "PNG", "MNG", "TGA", "SVG", "SWF"
};
String [] _archives = {
  "7Z", "TAR", "GZIP", "ZIP", "LZW", "ARJ", "RAR", "SDC"
};
//-----------------------------------------------------------------------------------------------

void setup() {
  size(1000, 500);
  ellipseMode(CENTER);
  rectMode(CENTER);
  textSize(23);
  //textAlign(CENTER, CENTER);

  for (int i = 0; i < nbCategories; i ++) {
    categoriesMedia[i] = new Bouton(20, 20 + (i*12), nomsCategoriesMedia[i]);
  }


  types = new StringDict();
  remplissageDictionnaireTypes();
  elements = new ArrayList();
  chemins = new StringList();
  gui = new GUI();
  // on récupère la liste de dossiers 2 niveaux au dessus du dossier du sketch
  currentDir = (sketchPath("../../"));
  //currentDir = ("/home/pclf/perso/Création_sonore");
  listFilesAndFilesSubDirectories(currentDir) ;

  println(nbElts);
  //  if (chemins != null) {
  //    for (int i=0; i<chemins.size(); i++) {
  //      println(chemins.get(i));
  //    }
  //  }
}

//×--------------------------------------------------
// méthode pour remplir le StringDict "types" à partir 
// des différents tableaux de String d'extensions
//×--------------------------------------------------

void remplissageDictionnaireTypes() {
  for (int i=0; i<_3D.length; i++) {
    types.set(_3D[i], "3D");
  }
  for (int i=0; i<_audio.length; i++) {
    types.set(_audio[i], "audio");
  }
  for (int i=0; i<_video.length; i++) {
    types.set(_video[i], "video");
  }
  for (int i=0; i<_texte.length; i++) {
    types.set(_texte[i], "texte");
  }
  for (int i=0; i<_code.length; i++) {
    types.set(_code[i], "code");
  }
  for (int i=0; i<_image.length; i++) {
    types.set(_image[i], "image");
  }
  for (int i=0; i<_archives.length; i++) {
    types.set(_archives[i], "archive");
  }
}

//×--------------------------------------------------
// récupère tous les fichiers contenus dans les dossiers 
// detectés précédemment
//×--------------------------------------------------

void listFilesAndFilesSubDirectories(String directoryName) {
  File directory = new File(directoryName);
  //println("truc");
  //get all the files from a directory
  File[] fList = directory.listFiles();
  // empêche la plantade si aucun fichier dans dossier
  if (fList != null) {
    for (File file : fList) {
      if (file.isFile()) {
        // à chaque fichier trouvé, on ajoute un element dans le Arraylist.
        String tempDataFile = file.getAbsolutePath();
        //println(tempDataFile);

        String [] titreTypeChemin = filtreNomsFichiers(tempDataFile);
        
        //c'est ici que je crée les objets elements
        elements.add(new Element(titreTypeChemin[0], titreTypeChemin[1], titreTypeChemin[2]));

        nbElts ++;
      } 
      else if (file.isDirectory()) {
        // on en profite pour remplir le StringList "chemins" avec les différents
        // dossiers dans lesquels nous avons des fichiers.
        String tempDataFolder = file.getAbsolutePath();
        lesPetitsChemins(tempDataFolder);

        listFilesAndFilesSubDirectories(file.getAbsolutePath());
      }
    }
  }
}
//×--------------------------------------------------
//  méthode pour l'analyse et le tri des chemins d'accès
//×--------------------------------------------------

String [] filtreNomsFichiers(String s) {
  String type, titre;

  String [] q = splitTokens(s, "/");
  String name = q[q.length-1];
  String camino = s.replace(name, "");
  String [] u = splitTokens(name, ".");

  titre = u[0];
  if (u.length > 1) {
    type = u[1];

    String r = types.get(type.toUpperCase());
    if (r != null) {
      type = r;
    }
    else {
      type = "inconnu";
    }
  }
  else {
    type = "inconnu";
  }

  String [] mesDonnees = {
    name, type, camino
  };

  return mesDonnees;
}

void lesPetitsChemins(String s) {
  if (!chemins.hasValue(s)) {
    chemins.append(s);
  }
}

void draw() {
  background(255);
  for (int i=0; i<elements.size(); i++) {
    Element element = elements.get(i);
    element.interact();
    element.affiche();
  }
  gui.interact();
  gui.affiche();
}

