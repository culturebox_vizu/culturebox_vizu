class GUI {


  GUI() {
    
  }

  void affiche() {
    fill(#FF8C0F);
    rect(60, 60, 100, 100);
    fill(255);
    for (int i = 0; i < nbCategories; i ++) {
      categoriesMedia[i].affiche();
    }
  }

  void interact() {
    for (int i = 0; i < nbCategories; i ++) {
      categoriesMedia[i].interact();
    }
  }
}

