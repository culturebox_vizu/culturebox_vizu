class Bouton {
  float x, y, taille;
  String nom;
  int compteurOn = 0;
  boolean toggle = false;

  Bouton(float _x, float _y, String _nom) {
    x = _x;
    y = _y;
    taille = 10;
    nom = _nom;
  }

  void interact() {
    if (overRect(x, y, taille, taille) && mousePressed) {
      if (compteurOn == 0) {
        toggle =! toggle;
        compteurOn ++;
      }
    }
    else {
      compteurOn = 0;
    }
  }

  void affiche() {
    fill(255);
    rect(x, y, taille, taille);
    fill(0);
    textSize(12);
    text(nom, x + (taille * 2)/3, y + (taille /2));
    if (toggle) {
      stroke(0);
      line(x - taille/2, y - taille/2, x + taille/2, y + taille/2);
      line(x - taille/2, y + taille/2, x + taille/2, y - taille/2);
    }
  }

  boolean overRect(float x, float y, float w, float h) {
    if (mouseX >= x-w/2 && mouseX <= x+w/2 && 
      mouseY >= y-h/2 && mouseY <= y+h/2) {
      return true;
    } 
    else {
      return false;
    }
  }
}

