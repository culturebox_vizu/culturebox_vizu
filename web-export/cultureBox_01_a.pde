String currentDir;
File directory;

void setup() {

  currentDir = (sketchPath("../../"));
  directory = new File(currentDir);

  //get all the files from a directory
  File[] fList = directory.listFiles();
  for (File file : fList) {
    if (file.isDirectory()) {
      println(file.getName());
    }
  }
}

void draw() {
  background(0);
  File[] fList = directory.listFiles();
  for (File file : fList) {
    if (file.isDirectory()) {
      text(file.getName(), 10,50);
    }
  }
}


