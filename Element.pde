class Element {
  String titre, type, chemin;
  color c;
  PVector pos;
  float taille;
  boolean afficheInfos = false;


  Element(String _titre, String _type, String _chemin) {
    titre = _titre;
    type = _type;
    chemin = _chemin;

    // pas très classe tout ça, mais fatigué là...
    for (int i=0; i<nbCategories; i++) {
      if (categoriesMedia[i].nom != null) {
        if (categoriesMedia[i].nom.equals(type)) {
          c = color(couleurCategoriesMedia[i], 125);
        }
      }
    }

    pos = new PVector(random(width), random(height));
    taille = 15;//titre.length()*5;
  }

  void init() {
  }

  void affiche() {


    noStroke();
    //if(type.equals(
    for (int i=0; i<nbCategories; i++) {
      if (categoriesMedia[i].nom != null) {
        if (categoriesMedia[i].nom.equals(type)) {
          if (categoriesMedia[i].toggle) {

            if (afficheInfos) {
              fill(0);
              text(titre, pos.x + 10, pos.y - (taille / 2 ));
              fill(#CE1919);
              text(type, pos.x + 10, pos.y + (taille / 2));
              //fill(#67E034);
              //text(chemin, pos.x + 10, pos.y + (2.5*taille));
            }

            fill(c);
            ellipse(pos.x, pos.y, taille, taille);
          }
        }
      }
    }
  }

  void deplace() {
  }

  void interact() {
    if (overCircle(pos.x, pos.y, taille)) {
      afficheInfos = true;
    }
    else {
      afficheInfos = false;
    }
  }

  boolean overCircle(float x, float y, float diameter) {
    float disX = x - mouseX;
    float disY = y - mouseY;
    if (sqrt(sq(disX) + sq(disY)) < diameter/2 ) {
      return true;
    } 
    else {
      return false;
    }
  }
}

